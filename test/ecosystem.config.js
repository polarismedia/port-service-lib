module.exports = {
    apps : [{
        name: "testapplication",
        script: "",
        cwd: "",
        instances: 1,
        detached: true,
        env: {
            NODE_ENV: 'development',
            SERVER_ENV: 'loc'
        },
        env_develop: {
            NODE_ENV: 'development',
            SERVER_ENV: 'dev'
        },
        env_acc: {
            NODE_ENV: 'production',
            SERVER_ENV: 'acc'
        },
        env_acceptance: {
            NODE_ENV: 'production',
            SERVER_ENV: 'acc'
        },
        env_master: {
            NODE_ENV: 'production',
            SERVER_ENV: 'pro'
        }
    },
    {
        name: "test2application",
        script: "",
        cwd: "",
        instances: 1,
        detached: true,
        env: {
            NODE_ENV: 'development',
            SERVER_ENV: 'loc'
        },
        env_develop: {
            NODE_ENV: 'development',
            SERVER_ENV: 'dev'
        },
        env_acc: {
            NODE_ENV: 'production',
            SERVER_ENV: 'acc'
        },
        env_acceptance: {
            NODE_ENV: 'production',
            SERVER_ENV: 'acc'
        },
        env_master: {
            NODE_ENV: 'production',
            SERVER_ENV: 'pro'
        }
    }]
};