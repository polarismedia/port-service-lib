const portService = require('port-service-lib');


describe('Port config:', () => {

    // getPortConfig()

    test('getPortConfig: should return port 4005', () => {
		expect(portService.getPortConfig()).toBe(4005);
	});

	test('getPortConfig: should return port 4005 with name param', () => {
		expect(portService.getPortConfig('testapplication')).toBe(4005);
    });
    
    test('getPortConfig: should return port 4006 with name param', () => {
		expect(portService.getPortConfig('test2application')).toBe(4006);
    });
    
    test('getPortConfig: should return undefined', () => {
		expect(portService.getPortConfig('test3application')).toBeUndefined;
	});
});

describe('Proxy config:', () => {

    // getProxyConfig()
    const testProxyObj = { proxy : { host: '192.168.101.201', port: 8080 } };
    test('getProxyConfig: should return test proxy object', () => {
		expect(portService.getProxyConfig()).toEqual(testProxyObj);
	});

    const emptyProxyObj = { proxy : { host: '', port: 0 } };
	test('getProxyConfig: should return empty proxy object', () => {
		expect(portService.getProxyConfig('testEmpty')).toEqual(emptyProxyObj);
	});
});


