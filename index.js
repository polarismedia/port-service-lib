/**
 * Lib to read config from port-service and return registerd ports for services.
 * Uses ecosystem.config.js file to find services to lookup.
 * When application is registed with port-serive the service-config.json file will contain registed details.
 * 
 * Returns undefined when service not registered.
 * 
 * Import: 
 *      const givenPortNumber = require('port-service-lib').getPortConfig();
 *      const givenPortNumber = require('port-service-lib').getPortConfig('name');
 * 
 *      const proxy = require('port-service-lib').getProxyConfig();
 * 
 * Example: 
 *      const port = givenPortNumber ? givenPortNumber : 3000;
 */
const fs = require('fs');
const isNotLocalBuild = process.env.SRV_ENV && process.env.SRV_ENV !== 'loc';
const isNotLocalBuildErrorMsg = "SRV_ENV is not set or is loc - (localbuild), will use default.";
const pathToConfig = process.env.SRV_ENV === 'test' ? '/test/' : '/../../';

/**
 * Get port registered for service by port-service.
 * 
 * @param {String} name Optional
 * @returns {Number} Will return undefined if exception or local-build 
 */
function getPortConfig(name) {
    try {
        if(isNotLocalBuild) {
            // Read ecosystem config.
            const ecosystemConfig = require(__dirname + pathToConfig + 'ecosystem.config.js');
            
            // Retrive all service names to array.
            const serviceNames = Array.from(ecosystemConfig.apps, app => app.name);

            // Read port-service config file.
            const serviceConfigFile = JSON.parse(fs.readFileSync(__dirname + pathToConfig + 'service-config.json', 'utf-8'));

            // Map ports with services from ecosystem.
            let serviceConfig = [];
            serviceNames.forEach((service) => {
                // Find registered service
                serviceConfigFile.services.forEach((serviceConf) => {
                    if(service === serviceConf.name) {
                        // Map name with given port.
                        serviceConfig[service] = serviceConf.port;
                    }
                });

                // Check if all services is registered
                if(!serviceConfig[service]) {
                    throw 'Application needs to be registered with port-service for all services! Please run register command';
                }
            });

            // Return port for named service.
            if(name) {
                if(serviceConfig[name]) {
                    return serviceConfig[name];
                }
                else {
                    console.error('Service name not found.. ' + name.toString());
                    return undefined;
                }
            }
            // Return port for first registed service.
            else {
                if(serviceConfig[serviceNames[0]]) {
                    return serviceConfig[serviceNames[0]];
                }
                else {
                    console.error('No service config found in port-service');
                    return undefined;
                }
            }
        }
        else {
            throw isNotLocalBuildErrorMsg;
        }
    }
    catch(e) {
        console.error('port-service-lib : getPortConfig() - ', e.toString());
        return undefined;
    }
}

/**
 * Return proxy config object ready for axios -> tunnel.httpsOverHttp()
 * Will return empty model object if exception or is localbuild.
 * 
 * @returns {Map<String,String|Number>} proxy: {
        host: 'xxx.xx.xxx.xx',
        port: xx
    }
 */
function getProxyConfig(param) {
    try {
        if(isNotLocalBuild) {
            // Read ecosystem config.
            const proxyConfigFile = JSON.parse(fs.readFileSync(__dirname + pathToConfig + 'service-proxy.json', 'utf-8'));

            // to be able to test crash - call undefined env.
            const envName = process.env.SRV_ENV === 'test' && param === 'testEmpty' ? 'test2' : process.env.SRV_ENV;

            const proxyObj = {
                proxy: {
                    host: proxyConfigFile[envName].host,
                    port: proxyConfigFile[envName].port
                }
            };

            return proxyObj;
        }
        else {
            throw isNotLocalBuildErrorMsg;
        }
    }
    catch(e) {
        console.error('port-service-lib : getProxyConfig() - ', e.toString());
        return { proxy : { host: '', port: 0 } };
    }
}

module.exports = {
    /**
     * Get port registered for service by port-service.
     * 
     * @param {String} name Optional
     * @returns {Number} Will return undefined if exception or local-build 
     */
    getPortConfig,
    /**
     * Return proxy config object ready for axios -> tunnel.httpsOverHttp()
     * Will return empty model object if exception or is localbuild.
     * 
     * @returns {Map<String,String|Number>} proxy: {
            host: 'xxx.xx.xxx.xx',
            port: xx
        }
    */
    getProxyConfig
};