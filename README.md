# Port Servive Lib

Lib to read config from port-service and return registerd ports for services and proxy config for server.
Uses ecosystem.config.js file to find services to lookup.
When application is registed with port-serive the service-config.json file will contain registed details.

Returns undefined when service not registered.

#### Import:
      const givenPortNumber = require('port-service-lib').getPortConfig(); - Return first registered if any.
      const givenPortNumber = require('port-service-lib').getPortConfig('name'); - Return named registered if exists.
  
      const proxy = require('port-service-lib').getProxyConfig(); - Returns config object with model: { proxy: { host: '', port: 0 }}

#### Example: 
      const givenPortNumber = require('port-service-lib').getServiceConfig();
      const port = givenPortNumber ? givenPortNumber : 3000;

      const proxy = require('port-service-lib').getProxyConfig();
      const proxyAgent = tunnel.httpsOverHttp(proxy);